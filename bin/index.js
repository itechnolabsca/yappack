#!/usr/bin/env node

const chalk = require('chalk');
const args = process.argv;
const linter = require('yaplint/build/linter');

const help = () => {
  console.log(`
${chalk.yellow('Usage:')}

yapreact [command]

Commands:

build : Create production build of the app
start : Start development server on env.PORT (default 3000)
lint  : Run linting on complete project
eject-eslint : Create .eslintrc for project so editors can use it
`);
};

if (args.length < 3) {
  console.error(chalk.red('Invalid Usage.'));
  help();
}

const command = args[2];
const justBuildIt = Boolean(process.env.I_AM_STUPID);

switch (command) {
  case 'start':
    require('../builder/scripts/start');
    break;

  case 'build':
    // const lintReport = linter.run();
    const lintReport = { errorCount: 0, warningCount: 0 };
    if (justBuildIt || (lintReport.errorCount === 0 && lintReport.warningCount <= 10)) {
      require('../builder/scripts/build');
    } else {
      console.log(chalk.red('Linting failed. Please clear linting issues and retry.'));
    }
    break;

  case 'lint':
    // ignore command line flags and shit
    const locs = args.slice(3).filter((p) => p[0] !== '-');
    linter.run(locs);
    break;

  case 'eject-eslint':
    linter.eject();
    break;

  default:
    console.error(chalk.red(`Unknown Command: ${command}`));
    help();
}
